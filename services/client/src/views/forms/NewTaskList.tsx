import { KInput } from '@/components/Input';
import { create } from '@/services/taskList';
import { MileStone, TaskList } from '@kangile/core';
import { InvalidStringInput } from '@kangile/core/src/exceptions/input.exception';
import { Button, Flex } from 'antd';
import { useMemo, useRef, useState } from 'react';
import { useParams } from 'react-router-dom';

export function NewTaskListForm(props: NewTaskListFormProps) {
	const [data, setData] = useState({ name: '', description: '' });
	const [error, setError] = useState({
		name: null,
	});
	const model = useRef(initTaskList());
	const params = useParams();

	const isReadyToSubmit = useMemo(() => {
		if (data.name && Object.values(error).every(v => !v)) {
			return true;
		} else {
			return false;
		}
	}, [data, error]);

	function setName(e: any) {
		const value = e.target.value;

		setData(prev => Object.assign({}, prev, { name: value }));

		try {
			model.current.name = value;
			setError(e => ({ ...e, name: null }));
		} catch (error: InvalidStringInput | any) {
			setError(e => ({ ...e, name: error }));
		}
	}

	function setDescription(e: any) {
		model.current.description = e.target.value;
		setData(prev =>
			Object.assign({}, prev, { description: e.target.value }),
		);
	}

	async function submit() {
		const mileStone = new MileStone();
		mileStone.id = params.milestoneId as string;

		model.current.mileStone = mileStone;
		try {
			await create(model.current);
			props.onSubmit();
		} catch (error) {
			// * NEEDPOPUP
			console.log(error);
		}
	}

	function cancel() {
		props.onClose();
	}

	return (
		<Flex vertical gap={'large'}>
			<Flex vertical gap={0}>
				<KInput
					label="Name"
					value={data.name}
					onChange={setName}
					description="No special characters and not empty"
					error={error.name}
					required
				/>
				<KInput
					label="Description"
					value={data.description}
					onChange={setDescription}
				/>
			</Flex>
			<Flex gap={'small'}>
				<Button
					disabled={!isReadyToSubmit}
					type="primary"
					onClick={submit}
				>
					Confirm
				</Button>
				<Button onClick={cancel}>Cancel</Button>
			</Flex>
		</Flex>
	);
}

export type NewTaskListFormProps = {
	onSubmit: () => void;
	onClose: () => void;
};

function initTaskList() {
	const tl = new TaskList();
	return tl;
}
