import { Table } from 'antd';
import { ColumnsType } from 'antd/es/table';

export type DataListProps = {
	data: any[];
	columnsDef: ColumnsType<any> | undefined;
	loading: boolean;
};

export const DataList = (props: DataListProps) => {
	if (props.loading) {
		return <h1>Loading</h1>;
	}

	return (
		<Table
			dataSource={props.data}
			columns={props.columnsDef}
			className="w-full"
		/>
	);
};
