import { useEffect, useRef, useState } from 'react';
import { Table } from 'antd';
import { getMileStoneByUserId } from '@/services/milestone';
import dayjs from 'dayjs';
import { Link } from 'react-router-dom';
import { ColumnsType } from 'antd/es/table';

const columns = [
	{
		title: 'Name',
		dataIndex: 'name',
		key: 'name',
		render: (value: any, data: any, index: any) => {
			return <Link to={`/projects/${data.id}`}>{value}</Link>;
		},
	},
	{
		title: 'Open date',
		dataIndex: 'open_date',
		key: 'open_date',
	},
	{
		title: 'Deadline',
		dataIndex: 'deadline',
		key: 'deadline',
	},
];

export type DataListProps = {
	data: any[];
	columnsDef: ColumnsType<any> | undefined;
	loading: boolean;
};

export const DataList = (props: DataListProps) => {
	const data = useRef<any>([]);
	const [loading, setLoading] = useState(true);

	useEffect(() => {
		(async () => {
			data.current = (await getMileStoneByUserId())?.map(item => ({
				...item,
				date_created: dayjs(item.date_created).format('YYYY/MM/DD'),
			}));
			console.log(data.current);
			setLoading(false);
		})();
	}, []);

	if (loading) {
		return <h1>Loading</h1>;
	}
	return (
		<Table
			dataSource={data.current}
			columns={columns}
			className="w-full"
			onRow={(record, rowIndex) => {
				return {
					onClick: event => {
						console.log(event);
					}, // click row
				};
			}}
		/>
	);
};
