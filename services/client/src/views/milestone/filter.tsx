import { useCallback } from "react";

import { Flex, Select } from "antd";

import Search from "antd/es/input/Search";

export function MilestoneFilter() {
    const onSearch = useCallback((e: any) => console.log(e), [])
    const handleChange = useCallback((e: any) => console.log(e), [])

    return <> 
    <Flex>
        {/* <Flex  className="w-full" > */}
        {/* <Select
      showSearch
      value={value}
      placeholder={props.placeholder}
      style={props.style}
      defaultActiveFirstOption={false}
      suffixIcon={null}
      filterOption={false}
      onSearch={handleSearch}
      onChange={handleChange}
      notFoundContent={null}
      options={(data || []).map((d) => ({
        value: d.value,
        label: d.text,
      }))}
    /> */}
        {/* </Flex> */}
        <Flex align="center" justify="end" gap="middle" className="w-full" >
            <Search onSearch={onSearch} placeholder="Type your milestone name..." style={{ width: 300 }} />
            <Select
                defaultValue="all"
                style={{ width: 120 }}
                onChange={handleChange}
                options={[
                    { value: 'all', label: 'All' },
                    { value: 'actived', label: 'Actived' },
                    { value: 'on_progress', label: 'On Progressing' },
                 ]}
            />
			</Flex>
        </Flex>
    </>
}