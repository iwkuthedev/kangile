import { useEffect, useRef, useState } from 'react';
import { Table } from 'antd';
import { getProjectByUserId } from '@/services/project';
import dayjs from 'dayjs';
import { Link } from 'react-router-dom';

const columns = [
	{
		title: 'Name',
		dataIndex: 'name',
		key: 'name',
		render: (value: any, data: any, index: any) => {
			return <Link to={`/projects/${data.id}/-/milestone`}>{value}</Link>
		},
	},
	{
		title: 'Description',
		dataIndex: 'description',
		key: 'description',
	},
	{
		title: 'Status',
		dataIndex: 'status',
		key: 'status',
	},
	{
		title: 'Date Create',
		dataIndex: 'date_created',
		key: 'date_created',
	},
];

export const ProjectList = () => {
	const data = useRef<any>([]);
	const [loading, setLoading] = useState(true);

	useEffect(() => {
		(async () => {
			data.current = (await getProjectByUserId())?.map(item => ({
				...item,
				date_created: dayjs(item.date_created).format('YYYY/MM/DD'),
			}));
			console.log(data.current);
			setLoading(false);
		})();
	}, []);

	if (loading) {
		return <h1>Loading</h1>;
	}
	return (
		<Table
			dataSource={data.current}
			columns={columns}
			className="w-full"
			onRow={(record, rowIndex) => {
				return {
					onClick: event => {
						console.log(event);
					}, // click row
				};
			}}
		/>
	);
};
