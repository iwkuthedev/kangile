import { useCallback } from "react";

import { Flex, Select } from "antd";

import Search from "antd/es/input/Search";

export function ProjectFilter() {
    const onSearch = useCallback((e: any) => console.log(e), [])
    const handleChange = useCallback((e: any) => console.log(e), [])


    return <>
        <Flex align="center" justify="end" gap="middle" className="w-full" >
            <Search onSearch={onSearch} placeholder="Type your project name..." style={{ width: 300 }} />
            <Select
                defaultValue="all"
                style={{ width: 120 }}
                onChange={handleChange}
                options={[
                    { value: 'all', label: 'All' },
                    { value: 'actived', label: 'Actived' },
                    { value: 'on_progress', label: 'On Progressing' },
                ]}
            />
        </Flex>
    </>
}