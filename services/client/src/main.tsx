import ReactDOM from 'react-dom/client';
import { GlobalContext } from './stores';
import './index.css'
import { App } from './App'
    ReactDOM.createRoot(document.getElementById('root')!).render(
        <GlobalContext.Provider
        value={{ 
        }}
      >
        <App />
      </GlobalContext.Provider>,
    )
