import React, { useState } from 'react';
import {
    DesktopOutlined,
    FileOutlined,
    FileSyncOutlined,
    PieChartOutlined,
    PlusSquareOutlined,
    ProjectOutlined,
    QuestionCircleOutlined,
    TeamOutlined,
    UndoOutlined,
    UserOutlined,
} from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Avatar, Breadcrumb, Dropdown, Flex, Layout, Menu, theme } from 'antd';
import { Link, Outlet } from 'react-router-dom';

import $ from "./index.module.scss"
import { VerticalLogo } from '../components/Logo';
import Search from 'antd/es/input/Search';

const { Header, Content, Footer, Sider } = Layout;

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
): MenuItem {
    return {
        key,
        icon,
        children,
        label,
    } as MenuItem;
}

const items: MenuItem[] = [
    getItem(<Link to="projects">Projects</Link>, '1', <ProjectOutlined />),
    getItem(<Link to="backlogs">Backlogs</Link>, '2', <FileSyncOutlined />),
    getItem(<Link to="milestones">Actived sprints</Link>, '3', <UndoOutlined />),
    // getItem('User', 'sub1', <UserOutlined />, [
    //     getItem('Tom', '3'),
    //     getItem('Bill', '4'),
    //     getItem('Alex', '5'),
    // ]),
    // getItem('Team', 'sub2', <TeamOutlined />, [getItem('Team 1', '6'), getItem('Team 2', '8')]),
    // getItem('Files', '9', <FileOutlined />),
];

const infoItems: MenuProps['items'] = [
    {
        key: 1,
        label: <Link to="/about">About us</Link>
    },
    {
        key: 2,
        label: <Link to="/about">About us</Link>
    },
]

const addMenuItems: MenuProps['items'] = [
    {
        key: 1,
        label: <Link to="/projects/new">New project</Link>
    },
    {
        key: 2,
        label: <Link to="/userStory/new">New user story</Link>
    },
]

export const MainLayout: React.FC = () => {
    const [collapsed, setCollapsed] = useState(false);
    const {
        token: { colorBgContainer },
    } = theme.useToken();

    return (
        <Layout style={{ minHeight: '100vh' }}>
            <Header style={{ padding: 0, background: colorBgContainer, height: 42 }} >
                <Flex align='center' justify='space-between' style={{ height: '100%', width: '100%', padding: 4, paddingRight: 16 }} gap="middle">
                    <VerticalLogo textColor='dark' />
                    <Search placeholder='Type name your project' style={{ width: 200 }} />
                    <Flex style={{ flex: 1 }} align='center' justify='end' gap={"middle"}>
                        <Dropdown menu={{ items: addMenuItems }} placement="bottomRight" arrow={{ pointAtCenter: true }}>
                            <PlusSquareOutlined style={{ fontSize: '16px' }} />
                        </Dropdown>
                        <Dropdown menu={{ items: infoItems }} placement="bottomRight" arrow={{ pointAtCenter: true }}>
                            <QuestionCircleOutlined style={{ fontSize: '16px' }} />
                        </Dropdown>
                        <Avatar size={16} icon={<UserOutlined />} />
                    </Flex>
                </Flex>
            </Header>
            <Layout hasSider>
                <Sider style={{ background: colorBgContainer }} collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
                    <Menu defaultSelectedKeys={['1']} mode="inline" items={items} />
                </Sider>
                <Content style={{ margin: '0 16px' }}>
                    <div className={$['main-content']} >
                        <Outlet />
                    </div>
                </Content>
            </Layout>
        </Layout>
    );
};