import { useId, useMemo } from 'react';

import { Flex, Input, InputProps, InputRef, Typography } from 'antd';

const { Paragraph } = Typography;

type KInputProps = {
	label: string;
	value: string;
	error?: any;
	required?: boolean;
	onChange: React.ChangeEventHandler<HTMLInputElement> | undefined;
	description?: string;
} & InputProps &
	React.RefAttributes<InputRef>;

export function KInput(props: KInputProps) {
	const id = useId();
	const {
		label,
		value,
		onChange,
		style,
		error,
		className,
		required,
		description,
		...inputProps
	} = props;

	const errorMsg = useMemo(() => {
		switch (error?.message) {
			case 'SPECIAL_CHARACTERS':
				return 'No special characters!';
			case 'EMPTY':
				return 'Not empty!';
			case 'NAME_EMPTY':
				return 'Field name can not empty!';
			default:
				return null;
		}
	}, [error]);

	return (
		<Flex
			vertical
			align="start"
			justify="start"
			style={style}
			className={className}
		>
			<label htmlFor={id} className="flex flex-row gap-1">
				<Paragraph strong>{label}</Paragraph>
				{required ? <span className="text-red-600">*</span> : <></>}
			</label>
			<Input id={id} value={value} onChange={onChange} {...inputProps} />
			{description && !errorMsg ? (
				<Paragraph italic> {description}</Paragraph>
			) : errorMsg ? (
				<Paragraph italic className="text-red-500">
					{errorMsg}
				</Paragraph>
			) : (
				<></>
			)}
		</Flex>
	);
}
