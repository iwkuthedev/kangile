import Title from "antd/es/typography/Title"
import "./index.scss"
import { Link } from "react-router-dom"

type VerticalLogoProps = {
    textColor: "dark" | "light"
}

export const VerticalLogo = (props: VerticalLogoProps) => {
    const textColor = props.textColor === 'dark' ? "#fff" : "#000"
    const background = props.textColor === "dark" ? "#001529" : "#fff"

    return <Link to="/" className="logo-vertical" style={{background}}>
        {/* <img src="/logo.png" alt="kangle-logo" className="logo-vertical__logo" /> */}
        <Title level={5} style={{ margin: 0, color: textColor }}>Kangle</Title>
    </Link>
}