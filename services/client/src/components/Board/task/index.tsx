import { SnippetsOutlined } from '@ant-design/icons';
import { Avatar, Flex } from 'antd';

export type TaskProps = {
	name: string;
	id: string;
};

export function TaskItem(props: TaskProps) {
	return (
		<Flex vertical gap={12} className="p-2 border-l-4 border border-gray-300 border-l-green-600 border-solid w-52">
			<h4 className="m-0">
				<SnippetsOutlined size={6} /> {props.id.slice(-10)}/ {props.name}
			</h4>
			<Flex align="center" gap={'small'}>
				<Avatar size={'small'} /> Pham Nhat Tien
			</Flex>
            <div className=' grid grid-cols-2'>
                <span>Effort</span>
                <strong>4</strong>
                <span>State</span>
                <span>To Do</span>
            </div>
		</Flex>
	);
}
