/**
 * priority: "HIGH" | "MEDIUM" | "LOW"
 * deadline: Date
 *
 * 1 level of priority = 1 week
 */

import dayjs, { Dayjs } from 'dayjs';

type PriorityType = 'HIGH' | 'MEDIUM' | 'LOW';

type Task = {
	priority: PriorityType;
	deadline: Dayjs;
	score?: number;
};

function priorityToScore(priority: PriorityType) {
	switch (priority) {
		case 'HIGH':
			return 2;
		case 'MEDIUM':
			return 1;
		default:
			return 0;
	}
}

function sort(arr: Task[]) {
	var len = arr.length;
	var swapped;
	do {
		swapped = false;
		for (var i = 0; i < len - 1; i++) {
			if ((arr[i].score || 0) > (arr[i + 1].score || 0)) {
				// Swap arr[i] and arr[i+1]
				var temp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = temp;
				swapped = true;
			}
		}
	} while (swapped);

	return arr;
}

function computeScores(tasks: Task[]) {
    // S1
	const scores = tasks.map(item => {
        // S2
		const daysLeft = dayjs().diff(dayjs(item.deadline), 'dates');
        const score =  daysLeft + 7 * priorityToScore(item.priority)
		return Object.assign({}, item, {
			score
		});
	});

    return scores
}
