function filterList(list: string[], searchTerm: string) {
	const searchTermWords = searchTerm
		.split(' ')
		.map(word => word.toLowerCase());
	const filteredList = [];

	for (const item of list) {
		const itemLower = item.toLowerCase();

		if (searchTermWords.every(word => itemLower.includes(word))) {
			filteredList.push(item);
		}
	}

	return filteredList;
}
