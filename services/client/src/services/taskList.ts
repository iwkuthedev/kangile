import { TaskList } from '@kangile/core';
import { directusClient } from './directus';
import {
	createItem,
	createRelation,
	readItems,
	updateItem,
} from '@directus/sdk';

export async function create(taskList: TaskList) {
	console.log(taskList.mileStone.id);
	console.log(taskList.description);
	const data = {
		name: taskList.name,
		description: taskList.description,
	};

	try {
		const res = await directusClient.request(createItem('tasklist', data));

		await directusClient.request(
			createItem('milestone_tasklist_1', {
				milestone_id: { id: taskList.mileStone.id },
				tasklist_id: res.id,
			}),
		);
	} catch (error) {
		console.log(error);
	}
}

export async function getTaskListByMileStoneId(id: string) {
	try {
		const res = await directusClient.request(
			readItems('milestone', {
				fields: [
					'tasklists.tasklist_id.*',
					'*',
					'tasklists.tasklist_id.tasks.task_id.*',
				],
				filter: {
					id,
				},
			}),
		);
		console.log(res);
		return (
			res?.[0]?.tasklists?.map((item: any) =>
				Object.assign({}, item.tasklist_id, {
					tasks: item?.tasklist_id?.tasks?.map((i: any) => {
						console.log(i)
						return i.task_id;
					}),
				}),
			) || []
		);
	} catch (error) {
		console.log(error);
	}
}
