import { Project } from '@kangile/core';
import { directusClient } from './directus';
import { createItem, readItem, readItems } from '@directus/sdk';
import { getCookie } from '@/utils/cookies';

export async function create(project: Project) {
	try {
		const created_by = getCookie('kagile-id');
		await directusClient.request(
			createItem('project', {
				name: project.name,
				description: project.description,
				status: project.status,
				created_by,
			}),
		);
	} catch (error) {
		console.log(error);
	}
}

export async function getProjectByUserId(name ?: any , status ? : any ) {
	try {
		const created_by = getCookie('kagile-id');
		return await directusClient.request(
			readItems('project', {
				filter: {
					created_by,
					
				},
			}),
		);
	} catch (error) {
		console.log(error);
	}
}


export async function getProjectById(id ?: string) {
	try {
		return await directusClient.request(
			readItem('project', id),
		);
	} catch (error) {
		console.log(error);
	}
}