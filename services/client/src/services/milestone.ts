import { getCookie } from '@/utils/cookies';
import { directusClient } from './directus';
import { createItem, readItems } from '@directus/sdk';
import { MileStone } from '@kangile/core';

export async function create(ms: MileStone) {
	try {
		const created_by = getCookie('kagile-id');
		const data = { name: ms.name, created_by , project : ms.projectId };
		if (ms.openDate) {
			Object.assign(data, {
				open_date: ms.openDate.toISOString(),
			});
		}

		if (ms.deadline) {
			Object.assign(data, {
				deadline: ms.deadline.toISOString(),
			});
		}
		console.log(ms)
		await directusClient.request(
			createItem('milestone', data),
		);
	} catch (error) {
		console.log(error);
	}
}

export async function getMileStoneByUserId() {
	try {
		const created_by = getCookie('kagile-id');
		return await directusClient.request(
			readItems('milestone', {
				filter: {
					created_by,
				},
			}),
		);
	} catch (error) {
		console.log(error);
	}
}


export async function getMileStoneByProjectId(id : string | undefined ) {
	try {
		const created_by = getCookie('kagile-id');
		return await directusClient.request(
			readItems('milestone', {
				filter: {
					created_by,
					project : id
				},
			}),
		);
	} catch (error) {
		console.log(error);
	}
}
