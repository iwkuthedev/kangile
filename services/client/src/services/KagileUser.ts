import { directusClient } from './directus';
import { createItem, readItem, readItems } from '@directus/sdk';

export async function createKangileUser(firebase_id: string, username: string) {
	try {
		const user = await directusClient.request(
			createItem('kangile_user', {
				firebase_id,
				username,
				status: 'actived',
			}),
		);

		return user;
	} catch (error) {
		console.log(error);
	}
}

export async function getKangileUser(firebase_id: string) {
	try {
		const user = await directusClient.request(
			readItems('kangile_user', {
				filter: {
					firebase_id,
				},
			}),
		);

		return user?.[0];
	} catch (error) {
		console.log(error);
	}
}
