import { createUserWithEmailAndPassword, signInWithEmailAndPassword } from 'firebase/auth';
import { auth } from '.';
import { setCookie } from '@/utils/cookies';
import { createKangileUser, getKangileUser } from './KagileUser';
import { directusClient } from './directus';
import { readItem } from '@directus/sdk';

export async function login(email: string, password: string) {
	try {
		const userCredential = await signInWithEmailAndPassword(auth, email, password)
		const user = userCredential.user;

		//@ts-ignore
		const { uid, accessToken } = user;

		const kagileUser = await getKangileUser(uid)

		setCookie('token', accessToken);
		setCookie('uid', uid);
		setCookie('kagile-id', kagileUser?.id);
	} catch (error) {
		console.log(error)
		// !-- sau se doi 
		throw error
	}
}

export async function register(
	username: string,
	email: string,
	password: string,
) {
	try {
		const userCredentials = await createUserWithEmailAndPassword(
			auth,
			email,
			password,
		);
		const user = userCredentials.user;
		//@ts-ignore
		const { uid, accessToken } = user;

		createKangileUser(uid, username);
	} catch (error) {
		console.log(error);
		throw new Error('! sẽ đổi sau');
	}
}
