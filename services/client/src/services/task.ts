import { Task } from '@kangile/core/src/models/task';
import { directusClient } from './directus';
import { createItem } from '@directus/sdk';

export async function create(task: Task, tasklistId: string) {
	const data = {
		name: task.name,
		description: task.description,
		status: task.status || 'TODO',
	};

	try {
		const res = await directusClient.request(createItem('task', data));
		await directusClient.request(
			createItem('tasklist_task', {
				tasklist_id: {
					id: tasklistId,
				},
				task_id: res.id,
			}),
		);
	} catch (error) {
		console.log(error);
	}
}
