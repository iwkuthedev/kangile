import { Outlet, createBrowserRouter, useNavigate } from 'react-router-dom';
import { MainLayout } from '../layouts/main';
import { ProjectPage } from '@/pages/project';
import { Login } from '@/pages/auth/login';
import { Register } from '@/pages/auth/register';
import { ProjectNewPage } from '@/pages/project/new';
import { TaskNew } from '@/pages/task';
import { getCookie } from '@/utils/cookies';
import { useEffect } from 'react';
import { ProjectDetailPage } from '@/pages/project/detail';
import { MileStone } from '@/pages/project/milestone';
import { MileStoneNewPage } from '@/pages/project/milestone/new';
import { MileStoneDetail } from '@/pages/project/milestone/detail';
import NotFound from '@/pages/not-found';
import { MilestonePage } from '@/pages/milestones';
import { HomePage } from '@/pages';

export const router = createBrowserRouter([
	{
		element: <CheckAuthWrap />,
		children: [
			{
				element: <MainLayout />,
				children: [
					{
						path: '/',
						element: <HomePage/>
					},
					{
						path: '/about',
						element: (
							<>
								<h1>About</h1>
							</>
						),
					},
					{
						path: '/projects',
						element: <ProjectPage />,
					},
					{
						path: '/projects/new',
						element: <ProjectNewPage />,
					},
					{
						path: '/projects/:projectId/-/tasks/new',
						element: <TaskNew />,
					},
					{
						path: '/projects/:projectId/-/milestone',
						element: <MileStone />,
					},
					{
						path: '/projects/:projectId/-/milestone/new',
						element: <MileStoneNewPage />,
					},
					{
						path: '/projects/:projectId/-/milestone/:milestoneId',
						element: <MileStoneDetail />,
					},
					{
						path: '/projects/:projectId',
						element: <ProjectDetailPage />,
					},
					{
						path : '/milestones', 
						element : <MilestonePage/>
					},
					{ 
						path:"*" ,element: <NotFound />
					},
					
				],
			},
		],
	},
	{
		path: '/auth/login',
		element: <Login />,
	},
	{
		path: '/auth/register',
		element: <Register />,
	},
	{ 
		path:"*" ,element: <NotFound />
	},
]);

function CheckAuthWrap() {
	const navigate = useNavigate();
	useEffect(() => {
		const uid = getCookie('uid');
		if (!uid) {
			navigate('/auth/login');
		}
	}, []);

	return <Outlet />;
}
