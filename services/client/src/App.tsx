import { getCookie } from '@/utils/cookies';
import { Login } from './pages/auth/login';
import { RouterProvider, useNavigate } from 'react-router-dom';
import { router } from './route';

export function App() {
	const uid = getCookie('uid');
    return <RouterProvider router={router}/>
}
