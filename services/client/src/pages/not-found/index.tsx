import { Link } from 'react-router-dom'
import $ from "./index.module.scss"

export default function NotFound(props: any) {
  return (
    <div className={$.container}>
      <img src="/404.png" alt="" />
      <h2>Dường như trang bạn tìm kiếm không tồn tại</h2>
      <Link to="/">{LeftArrow} Về trang chủ</Link>
    </div>
  )
}

const LeftArrow = <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M15.5 19.92L8.98003 13.4C8.21003 12.63 8.21003 11.37 8.98003 10.6L15.5 4.08" stroke="white" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
</svg>

