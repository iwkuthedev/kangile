import "../login/index.scss"
import { Button, Input, Space } from "antd";
import { EyeInvisibleOutlined, EyeTwoTone, GooglePlusOutlined, KeyOutlined, UserOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import { Typography } from "antd"
import { useCallback, useState } from "react";
import { register } from "@/services/auth";
const { Title, Link: TextLink, Paragraph } = Typography

type RegisterData = {
    username: string,
    email: string,
    password: string
}

export function Register() {
    const [registerData, setRegisterData] = useState<RegisterData>({
        username: "",
        email: "",
        password: ""
    })

    const changeRegisterData = (key: 'username' | 'email' | 'password', value: string) => {
        setRegisterData(prev => ({ ...prev, [key]: value }))
    }

    const handleRegister = useCallback(async () => {
        try {
            await register(registerData.username, registerData.email, registerData.password)
        } catch (error) {
            console.log(error)
        }
    }, [registerData])

    return <div className="login">
        <div className="login__form">
            <Title level={1}>Đăng Ký</Title>
            <Space direction="vertical">
                <Input
                    value={registerData.username}
                    onChange={e => changeRegisterData('username', e.target.value)}
                    prefix={<UserOutlined />}
                    placeholder="Tên đăng nhập"
                />
                <Input
                    value={registerData.email}
                    onChange={e => changeRegisterData('email', e.target.value)}
                    prefix={<GooglePlusOutlined />}
                    placeholder="Email"
                />
                <Input.Password
                    value={registerData.password}
                    onChange={e => changeRegisterData('password', e.target.value)}
                    prefix={<KeyOutlined />}
                    placeholder="Mật khẩu"
                    iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                />
                <Link to="/"><TextLink>Quên mật khẩu</TextLink></Link>
            </Space>
            <Button size="large" type="primary" onClick={handleRegister}>Đăng ký ngay</Button>
            <Paragraph>Bạn đã có tài khoản. Đăng nhập <Link to="/auth/login">tại đây</Link></Paragraph>
        </div>
        <div className="login__thumnail">
            <img src="/loginthumnail.jpg" alt="" />
        </div>
    </div>
}