
import "./index.scss"
import { Button, Input, Space } from "antd";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import { Link } from "react-router-dom";
import { Typography } from "antd"
import { useCallback, useState } from "react";
import { login } from "@/services/auth";
const { Title, Link: TextLink, Paragraph } = Typography

type loginDataType = {
	email: string,
	password: string
}

export function Login() {
	const [loginData, setLoginData] = useState({
		email: '',
		password: ''
	})

	function changeLoginData(key: string, value: string) {
		setLoginData(prev => ({ ...prev, [key]: value }))
	}

	const handleLogin = useCallback(async () => {
		try {
			await login(loginData.email, loginData.password)
		} catch (error) {
			console.log(error)
		}
	}, [loginData])

	return (
		<>
			<div className="login">
				<div className="login__form">
					<Title level={1}>Đăng nhập</Title>
					<Space direction="vertical">
						<Input
							value={loginData.email}
							onChange={e => changeLoginData('email', e.target.value)}
							placeholder="Email"
						/>
						<Input.Password
							value={loginData.password}
							onChange={e => changeLoginData('password', e.target.value)}
							placeholder="Mật khẩu"
							iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
						/>
						<Link to="/"><TextLink>Quên mật khẩu</TextLink></Link>
					</Space>
					<Button onClick={handleLogin} size="large" type="primary">Đăng nhập ngay</Button>
					<Paragraph>Nếu bạn chưa có tài khoản đăng ký ngay. <Link to="/auth/register">Tại đây</Link></Paragraph>
				</div>
				<div className="login__thumnail">
					<img src="/loginthumnail.jpg" alt="" />
				</div>
			</div>
		</>
	);
}
