import Title from 'antd/es/typography/Title';
import { Button, Divider, Flex } from 'antd';
import { PlusOutlined, ProjectOutlined } from '@ant-design/icons';


import { Link } from 'react-router-dom';
import { DataList } from '@/views/DataList';
import { useVM } from './index.vm';
import { MilestoneFilter } from '@/views/milestone/filter';




export const MileStone: React.FC = () => {
	const { data,projectData, loading, columns } = useVM();

	return (
		<>
			<Flex align="flex-start" vertical>
				<Flex vertical className="w-full px-6">
					<Flex
						justify="space-between"
						align="center"
						className="w-full"
					>
						<Title level={2} style={{ width: 'fit-content' }}>
							<ProjectOutlined /> Project: {projectData.current.name}
						</Title>
						<Link to="new">
							<Button
								type="primary"
								icon={<PlusOutlined />}
								size="middle"
							>
								New MileStone
							</Button>
						</Link>
					</Flex>
					<MilestoneFilter/>
				</Flex>
				<Divider />
				<DataList
					columnsDef={columns}
					data={data.current}
					loading={loading}
				/>
			</Flex>
		</>
	);
};
