import { PlusSquareOutlined, RightOutlined } from '@ant-design/icons';
import { Task } from '@kangile/core/src/models/task';
import { Button, Flex } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import cls from 'classnames';
import { TaskItem } from '@/components/Board/task';
import { NewTaskListForm } from '@/views/forms/NewTaskList';
import { getTaskListByMileStoneId } from '@/services/taskList';
import { useParams } from 'react-router-dom';
import { NewTaskForm } from '@/views/forms/NewTask';

export function useVM() {
	const [open, setOpen] = useState<boolean>(false);
	const [refresh, setRefresh] = useState(0);
	const [loading, setLoading] = useState(true);
	const [expandColumn, setExpandColumn] = useState<string[]>([]);
	const data = useRef([]);
	const params = useParams();

	function refreshFunc() {
		setRefresh(prev => prev + 1);
	}

	useEffect(() => {
		(async () => {
			setLoading(true);
			const res = await getTaskListByMileStoneId(
				params.milestoneId as string,
			);
			data.current = res.map((item: any) =>
				Object.assign({}, item, {
					todo: item.tasks.filter(
						(task: any) => task.status === 'TODO',
					),
				}),
			);
			setLoading(false);
		})();
	}, [refresh]);

	const onCloseForm = () => {
		setOpen(false);
	};

	const onSubmitForm = () => {
		//! gọi refetch
		setOpen(false);
		refreshFunc();
	};

	const form = useRef(<></>);

	function handleAddTaskList() {
		form.current = (
			<NewTaskListForm onClose={onCloseForm} onSubmit={onSubmitForm} />
		);
		setOpen(true);
	}

	function handleAddTask(taskListId: string) {
		form.current = (
			<NewTaskForm
				onClose={onCloseForm}
				onSubmit={onSubmitForm}
				taskListId={taskListId}
			/>
		);
		setOpen(true);
	}

	const changeExpandColumn = useCallback(
		(id: string) => {
			if (expandColumn.some(item => item === id)) {
				setExpandColumn(prev => prev.filter(item => item !== id));
			} else {
				setExpandColumn(prev => prev.concat([id]));
			}
		},
		[expandColumn],
	);

	const columns = useMemo<ColumnType<any>>(
		() =>
			[
				{
					title: (
						<Flex align="center" gap={'small'}>
							Task List{' '}
							<Button onClick={handleAddTaskList}>+</Button>
						</Flex>
					),
					dataIndex: 'name',
					key: 'name',
					className: 'flex',
					width: '25%',
					render: (value: any, data: any) => {
						const expand = expandColumn.includes(data.id);
						return (
							<Flex
								onClick={() => changeExpandColumn(data.id)}
								vertical
								justify="start"
								className=" h-full cursor-pointer"
							>
								<Flex gap={8}>
									<RightOutlined
										className={cls({ 'rotate-90': expand })}
									/>
									{value}
								</Flex>
							</Flex>
						);
					},
				},
				{
					title: 'To Do',
					dataIndex: 'todo',
					key: 'todo',
					width: '25%',
					render: (value: any, data: any) => {
						if (expandColumn.includes(data.id)) {
							return (
								<Flex
									align="start"
									wrap="wrap"
									vertical
									gap={'large'}
								>
									{data.todo?.map((task: Task) => (
										<div>
											<TaskItem key={task.id} {...task} />
										</div>
									))}
									<Button
										onClick={() => handleAddTask(data.id)}
									>
										<PlusSquareOutlined />
										Add task
									</Button>
								</Flex>
							);
						} else {
							return <div>Close</div>;
						}
					},
				},
				{
					title: 'In Progress',
					dataIndex: 'doing',
					key: 'doing',
					width: '25%',
					render: (value: any, data: any) => {
						if (expandColumn.includes(data.id)) {
							return (
								<Flex
									align="start"
									wrap="wrap"
									vertical
									gap={'large'}
								>
									{data.doing?.map((task: Task) => (
										<TaskItem key={task.id} {...task} />
									))}
								</Flex>
							);
						} else {
							return <div>Close</div>;
						}
					},
				},
				{
					title: 'Done',
					dataIndex: 'done',
					key: 'done',
					width: '25%',
					render: (value: any, data: any) => {
						if (expandColumn.includes(data.id)) {
							return (
								<Flex
									align="start"
									wrap="wrap"
									vertical
									gap={'large'}
								>
									{data.done?.map((task: Task) => (
										<TaskItem key={task.id} {...task} />
									))}
								</Flex>
							);
						} else {
							return <div>Close</div>;
						}
					},
				},
			] as ColumnType<any>,
		[expandColumn],
	);

	return {
		handleAddTaskList,
		open,
		form: form.current,
		columns,
		loading,
		data: data.current,
	};
}
