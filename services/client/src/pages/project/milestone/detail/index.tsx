import { Drawer, Flex, Table } from 'antd';
import { Task } from '@kangile/core/src/models/task';
import { useVM } from './index.vm';

const task = new Task();
task.id = '123';
task.name = 'Lam phan 1';
task.status = 'TODO';

const task1 = new Task();
task1.id = '23';
task1.name = 'Lam tiep phan 2';
task.status = 'TODO';

export function MileStoneDetail() {

	const { handleAddTaskList, open, form, columns,data } = useVM();


	return (
		<>
			<Flex vertical>
				<h2>Filter</h2>
				<Table columns={columns} dataSource={data} />
			</Flex>
			<Drawer
				// title="New Task List"
				placement={'right'}
				closable={false}
				onClose={console.log}
				open={open}
				key={'right'}
			>
				{form}
			</Drawer>
		</>
	);
}
