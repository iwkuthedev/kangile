import { useRef, useState, useEffect } from "react";

import dayjs from "dayjs";

import {
  getMileStoneByProjectId,
  getMileStoneByUserId,
} from "@/services/milestone";
import { Link, useLocation, useParams } from "react-router-dom";
import { ColumnsType } from "antd/es/table";
import { getProjectById } from "@/services/project";
import { Badge, Tag } from "antd";

export function useVM() {
  const data = useRef<any>([]);
  const projectData = useRef<any>([]);
  const [loading, setLoading] = useState(true);
  const location = useLocation();
  const { projectId } = useParams();
  useEffect(() => {
    (async () => {
      data.current = (await getMileStoneByProjectId(projectId))?.map(
        (item) => ({
          ...item,
          date_created: dayjs(item.date_created).format("YYYY/MM/DD"),
        })
      );
      projectData.current = await getProjectById(projectId);

      setLoading(false);
    })();
  }, []);

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      render: (value: any, data: any, index: any) => {
        return <Link to={`${location.pathname}/${data.id}`}>{data.name}</Link>;
      },
    },
    {
      title: "Open date",
      dataIndex: "open_date",
      key: "open_date",
      render: (value: string) =>
        value ? dayjs(value).format("DD/MM/YYYY") : "None set",
    },
    {
      title: "Deadline",
      dataIndex: "deadline",
      key: "deadline",
      render: (value: string) =>
        value ? dayjs(value).format("DD/MM/YYYY") : "None set",
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      render: (value: string) => (
        <Tag color="#108ee9">{value}</Tag>
        
      ),
    },
  ] as ColumnsType<any>;

  return {
    data,
    projectData,
    loading,
    columns,
  };
}
