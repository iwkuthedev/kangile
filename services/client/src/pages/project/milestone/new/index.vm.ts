import { create } from '@/services/milestone';
import { MileStone } from '@kangile/core';
import { InvalidStringInput } from '@kangile/core/src/exceptions/input.exception';
import { MileStoneException } from '@kangile/core/src/exceptions/milestone.exceptioin';
import { Dayjs } from 'dayjs';
import { useMemo, useRef, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';

export function useVM() {
	const {projectId} = useParams() 
	const model = useRef(initMileStone(projectId as string ));

	const [data, setData] = useState<MileStone>(model.current);
	const [error, setError] = useState({
		name: null,
	});

	const navigate = useNavigate();
	const location = useLocation()

	function setName(e: any) {
		const value = e.target.value;
		setData(prev => ({ ...prev, name: value } as MileStone));
		try {
			model.current.name = value;
			setError(e => ({ ...e, name: null }));
		} catch (error: InvalidStringInput | any) {
			setError(e => ({ ...e, name: error }));
		}
	}

	function handleRangeChange(value: Dayjs[]) {
		try {
			model.current.openDate = value[0];
			model.current.deadline = value[1];
			setError(e => ({ ...e, name: null }));
		} catch (error: MileStoneException | any) {
			setError(e => ({ ...e, name: error }));
		}
	}

	const isReadyToSubmit = useMemo(() => {
		if (data.name && Object.values(error).every(v => !v)) {
			return true;
		} else {
			return false;
		}
	}, [data, error]);

	async function submit() {
		try {
			await create(model.current);
			console.log(location)
			navigate(location.pathname.replace('/new', ''))
		} catch (error) {
			console.log(error);
		}
	}

	return {
		data,
		setName,
		handleRangeChange,
		isReadyToSubmit,
		submit,
		error,
	};
}
function initMileStone(projectId: string) {
	const mileStone = new MileStone();
	mileStone.projectId = projectId
	return mileStone;
}
