import { KInput } from '@/components/Input';
import { Button, Flex, Typography, DatePicker } from 'antd';
import { Link } from 'react-router-dom';
import { useVM } from './index.vm';

const { Title, Paragraph } = Typography;
const { RangePicker } = DatePicker;

export function MileStoneNewPage() {
	const { data, setName,handleRangeChange, error, isReadyToSubmit, submit } = useVM();
	return (
		<Flex
			vertical
			align="start"
			justify="start"
			className="container mx-auto py-4 pt-4"
			gap={'large'}
		>
			<Flex
				align="center"
				justify="start"
				className="h-20 px-8"
				gap="middle"
			>
				<img
					src="/milestone.png"
					alt="new MileStone icon"
					className="h-20"
				/>
				<Flex vertical>
					<Title level={4}>Create New MileStone</Title>
					<Paragraph>
						Create milestones to mark important MileStone milestones
						and track progress effectively.
					</Paragraph>
				</Flex>
			</Flex>
			<Flex className="w-full container" vertical>
				<KInput
					label="What is your MileStone name"
					value={data.name}
					onChange={setName}
					description="No special characters and not empty"
					error={error.name}
					required
				/>
				{/* @ts-ignore */}
				<RangePicker onChange={handleRangeChange}/>
			</Flex>
			<Flex gap={'small'}>
				<Button
					type="primary"
					disabled={!isReadyToSubmit}
					onClick={submit}
				>
					Create Milestone
				</Button>
				<Link to="/MileStones">
					<Button>Cancel</Button>
				</Link>
			</Flex>
		</Flex>
	);
}
