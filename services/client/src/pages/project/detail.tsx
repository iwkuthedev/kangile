import { useParams } from "react-router-dom";

export function ProjectDetailPage() {
    const params = useParams() 

    return <h1>{params.projectId}</h1>
}