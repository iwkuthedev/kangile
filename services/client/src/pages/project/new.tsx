import { KInput } from '@/components/Input';
import { Button, Flex } from 'antd';
import { Typography } from 'antd';
import { useMemo, useRef, useState } from 'react';
import { Project } from '@kangile/core';
import { InvalidStringInput } from '@kangile/core/src/exceptions/input.exception';
import { create } from '@/services/project';
import { Link, useNavigate } from 'react-router-dom';

const { Title, Paragraph } = Typography;

function initProject() {
		const project = new Project();
		project.status = 'actived';
		return project;
}

export function ProjectNewPage() {
	const model = useRef(initProject());

	const [data, setData] = useState<Project>(model.current);
	const [error, setError] = useState({
		name: null,
	});

	const navigate = useNavigate()

	function setName(e: any) {
		const value = e.target.value;
		setData(prev => ({ ...prev, name: value } as Project));
		try {
			model.current.name = value;
			setError(e => ({ ...e, name: null }));
		} catch (error: InvalidStringInput | any) {
			setError(e => ({ ...e, name: error }));
			return;
		}
	}

	function setDescription(e: any) {
		const value = e.target.value;
		setData(prev => ({ ...prev, description: value } as Project));
		try {
			model.current.description = value;
			setError(e => ({ ...e, description: null }));
		} catch (error: InvalidStringInput | any) {
			setError(e => ({ ...e, description: error }));
			return;
		}
	}

	const isReadyToSubmit = useMemo(() => {
		if (data.name && Object.values(error).every(v => !v)) {
			return true;
		} else {
			return false;
		}
	}, [data, error]);

	async function submit() {
		try {
			await create(model.current)
			navigate('/projects')
		} catch (error) {
			console.log(error)
		}
	}

	return (
		<Flex
			vertical
			align="start"
			justify="start"
			className="container mx-auto py-4 pt-4"
			gap={'large'}
		>
			<Flex
				align="center"
				justify="start"
				className="h-20 px-8"
				gap="middle"
			>
				<img
					src="/new-project.png"
					alt="new project icon"
					className="h-20"
				/>
				<Flex vertical>
					<Title level={4}>Create New Project</Title>
					<Paragraph>
						Create a blank project to store your files, plan your
						work, and collaborate on code, among other things.
					</Paragraph>
				</Flex>
			</Flex>
			<Flex className="w-full container" vertical>
				<KInput
					label="Project name"
					value={data.name}
					onChange={setName}
					description="No special characters and not empty"
					error={error.name}
					required
				/>
				<KInput
					label="Description"
					value={data.description}
					onChange={setDescription}
				/>
			</Flex>
			<Flex gap={'small'}>
				<Button
					type="primary"
					disabled={!isReadyToSubmit}
					onClick={submit}
				>
					Create project
				</Button>
				<Link to='/projects'>
				<Button>Cancel</Button>
				</Link>
			</Flex>
		</Flex>
	);
}
