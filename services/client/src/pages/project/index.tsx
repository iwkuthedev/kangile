import Title from 'antd/es/typography/Title';
import { Button, Divider, Flex, Layout, theme } from 'antd';
import { PlusOutlined, ProjectOutlined } from '@ant-design/icons';

import { ProjectList } from '@/views/project/list';
import { ProjectFilter } from '@/views/project/filter';
import { Link } from 'react-router-dom';

export const ProjectPage: React.FC = () => {

	return (
		<>
			<Flex align="flex-start" vertical>
				<Flex vertical className="w-full px-6">
					<Flex
						justify="space-between"
						align="center"
						className="w-full"
					>
						<Title level={2} style={{ width: 'fit-content' }}>
							<ProjectOutlined /> Projects
						</Title>
						<Link to="/projects/new">
						<Button
							type="primary"
							icon={<PlusOutlined />}
							size="middle"
						>
							New Project
						</Button>
						</Link>
					</Flex>
					<ProjectFilter />
				</Flex>
				<Divider />
				<ProjectList />
			</Flex>
		</>
	);
};
