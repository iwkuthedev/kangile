import { getProjectByUserId } from "@/services/project";
import { HomeOutlined } from "@ant-design/icons";
import { Divider, Flex, Tabs, TabsProps, Typography } from "antd";
import dayjs from "dayjs";
import { useEffect, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
const { Title, Paragraph } = Typography;

export function HomePage() {
  const data = useRef<any>([]);
  const [loading, setLoading] = useState(true);
  const navigate = useNavigate();
  useEffect(() => {
    (async () => {
      data.current = (await getProjectByUserId())?.map((item) => ({
        ...item,
        date_created: dayjs(item.date_created).format("YYYY/MM/DD"),
      }));
      console.log(data.current);
      setLoading(false);
    })();
  }, []);

  const items: TabsProps["items"] = [
    {
      key: "1",
      label: "Projects",
      children: (
        <Flex gap={16}>
          {data.current.map((project: any) => {
            return (
              <Flex
                onClick={(event) => {
                  console.log(project.id);
                  navigate("/projects/" + project.id + "/-/milestone");
                }}
                vertical
                className=" p-4 shadow-[rgba(0,_0,_0,_0.24)_0px_3px_8px] w-96 bg-white rounded-md overflow-hidden text-ellipsis "
              >
                <Title level={3}>{project.name}</Title>
                <p className="text-ellipsis-3-row">{project.description}</p>
              </Flex>
            );
          })}
        </Flex>
      ),
    },
    {
      key: "2",
      label: "My works items ",
      children: (
        <Flex gap={16}>
          <p>My work items </p>
        </Flex>
      ),
    },
  ];

  if (loading) {
    return <h1>Loading</h1>;
  }
  return (
    <Flex vertical>
      <Flex>
        <Title level={1} style={{ width: "fit-content" }}>
          <HomeOutlined /> Home{" "}
        </Title>
      </Flex>
      <Divider />

      <Flex vertical>
        <Tabs defaultActiveKey="1" items={items} />
      </Flex>
    </Flex>
  )
}
