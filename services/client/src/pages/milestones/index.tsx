import Title from 'antd/es/typography/Title';
import { Button, Divider, Flex, Layout, theme } from 'antd';
import { PlusOutlined, ProjectOutlined } from '@ant-design/icons';

import { Link } from 'react-router-dom';
import { MilestoneFilter } from '@/views/milestone/filter';
import { DataList } from '@/views/milestone/list';

export const MilestonePage: React.FC = () => {

	return (
		<>
			<Flex align="flex-start" vertical>
				<Flex vertical className="w-full px-6">
					<Flex
						justify="space-between"
						align="center"
						className="w-full"
					>
						<Title level={2} style={{ width: 'fit-content' }}>
							<ProjectOutlined /> Milestones
						</Title>
						<Link to="/milestones/new">
						<Button
							type="primary"
							icon={<PlusOutlined />}
							size="middle"
						>
							New Project
						</Button>
						</Link>
					</Flex>
					<MilestoneFilter />
				</Flex>
				<Divider />
				<DataList />
			</Flex>
		</>
	);
};
