import { InvalidStringInput } from "../exceptions/input.exception"

type UserStatusType = 'actived' | 'disactive' | 'banned'

export class User {
    private _id: string
    private _username: string
    private _status: UserStatusType
    private _avatar: string
    constructor() {
        this._status = "actived"
    }
    get id(): string {
        return this._id
    }

    get username(): string{ 
        return this._username
    }

    get status(): UserStatusType {
        return this._status
    }

    get avatar(): string {
        return this._avatar
    }

    set id(str: string) {
        this._id = str
    }
    /**
     * @description set name
	 * @throws InvalidUserInput({ msg: 'EMPTY' }) - when user's name os empty
     */
    set username(str: string) {
        if(!str)
            throw new InvalidStringInput('EMPTY')
        this._username = str
    }
    /**
     * @description set status for user
     * @default 'actived'
     */
    set status(st: UserStatusType) {
        this._status = st
    }
    /**
     * @description set avatar for user
     */
    set avatar(str: string) {
        this._avatar=str
    }
}