import { Dayjs } from 'dayjs';
import { TaskList } from './taskList';
import { InvalidStringInput } from '../exceptions/input.exception';
import { MileStoneException } from '../exceptions/milestone.exceptioin';

export class MileStone {
	private _id: string
	private _name: string;
	private _openDate: Dayjs;
	private _deadline: Dayjs;
	private _taskLists: TaskList[];
	private _projectId : string

	/**
	 * @@default taskLists = []
	 */
	constructor() {
		this._taskLists = [];
	}

	get id(): string {
		return this._id
	}

	get name(): string {
		return this._name;
	}

	get taskLists(): TaskList[] {
		return this._taskLists;
	}

	get openDate(): Dayjs {
		return this._openDate;
	}
	get deadline(): Dayjs {
		return this._deadline;
	}

	set id(str: string) {
		this._id = str;
	}

	set projectId(id: string) {
		this._projectId = id;
	}

	get projectId(): string {
		return this._projectId;
	}

	/**
	 * @description set name for MileStone
	 * @throws InvalidStringInput({ msg: "EMPTY" }) - when string is ''
	 */
	set name(str: string) {
		if (str === '') throw new InvalidStringInput('EMPTY');
		this._name = str;
	}

	/**
	 * @description set openDate for mileStone
	 * @throws MileStoneError('DEADLINE_IS_BEFORE_OPENDATE') - when deadline is before open date
	 */
	set openDate(date: Dayjs) {
		if (this._deadline && this._deadline.isBefore(date))
			throw new MileStoneException('DEADLINE_IS_BEFORE_OPENDATE');
		this._openDate = date;
	}

	/**
	 * @description set deadline for mileStone
	 * @throws MileStoneError('DEADLINE_IS_BEFORE_OPENDATE') - when deadline is before open date
	 */
	set deadline(date: Dayjs) {
		if (this._openDate && date.isBefore(this._openDate))
			throw new MileStoneException('DEADLINE_IS_BEFORE_OPENDATE');
		this._deadline = date;
	}

	/**
	 * @description add tasklist into milestone
	 * + Validate before add into mileStone
	 * @throws MileStoneError('TAKSLIST_EXISTED') - when tasklist is already existed in milestone
	 * @param taskList
	 */
	addTaskList(taskList: TaskList) {
		if(this._taskLists.some(item => item.id === taskList.id)) {
			throw new MileStoneException('TASKLIST_EXISTED')
		}
		taskList.validate()
		this._taskLists.push(taskList);
	}

	/**
	 * @description remove tasklist from milestonew
	 * @throws MileStoneError('TASKLIST_NOT_FOUND') - when taskList not found in milestone
	 * @param id
	 */
	removeTaskList(id: string) {
		if(!this._taskLists.some(item => item.id === id))
		throw new MileStoneException('TASKLIST_NOT_FOUND')
		this._taskLists = this.taskLists.filter(item => item.id !== id);
	}
	validate() {}
}
