import { TaskListException } from '../exceptions/taskList.exception';
import { InvalidStringInput } from '../exceptions/input.exception';
import { MileStone } from './mileStone';
import { Task } from './task';

export class TaskList {
	private _id: string;
	private _name: string;
	private _description: string;
	private _tasks: Task[];
	private _milestone: MileStone;
	constructor() {
		this._tasks = []
	}

	get id(): string {
		return this._id;
	}
	get name(): string {
		return this._name;
	}
	get description(): string {
		return this._description;
	}
	get tasks(): Task[] {
		return this._tasks;
	}
	get mileStone(): MileStone {
		return this._milestone;
	}

	/**
	 * @description Set id for UserStory
	 */
	set id(str: string) {
		this._id = str;
	}

	/**
	 * @description Set name for UserStory
	 * @throws InvalidStringInput({ msg: "EMPTY" }) - when string is ''
	 */
	set name(str: string) {
		if(str.trim() === '') throw new InvalidStringInput("EMPTY")
		this._name = str
	}

	/**
	 * @description Set description for task
	 */
	set description(str: string) {
		this._description = str
	}

	/**
	 * @description add task into sprint:
	 * + Task is going to added to sprint
	 * + Task's status is going to set 'TO_DO'
	 * + Validate task before add into UserStory
	 * @throws InputTaskInvalid({ msg: 'ALREADY_EXIST' }) - when UserStory is already have this task
	 * @param task
	 */
	addTask(task: Task) {
		if(!task.status)
			task.status = "TODO"
		task.validate()
		this._tasks.push(task)
	}

	/**
	 * @description remove task from sprint
	 * @throws InputTaskInvalid({ msg: 'NOT_FOUND' }) - Task is not found in this UserStory
	 * @param id
	 */
	removeTask(id: string) {
		if(!this._tasks.some(item => item.id === id))
		throw new TaskListException('TASK_NOT_FOUND')
		this._tasks = this._tasks.filter(item => item.id !== id)
	}

	set mileStone(ms: MileStone) {
		this._milestone = ms
	}

	/**
	 * @throws TaskListException('NAME_EMPTY')- when tasklist's name is empty
	 */
	validate() {
		if(!this._name || this._name== "")  
		throw new TaskListException('NAME_EMPTY')
	}
}
