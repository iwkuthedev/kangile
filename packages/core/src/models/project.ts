import { containsSpecialCharacters } from '../utils/string';
import { InvalidStringInput } from '../exceptions/input.exception';
import { ProjectException } from '../exceptions/project.exception';

export type ProjectStatusType = 'actived' | 'deleted' | null;

export const PROJECT_STATUS: {
	actived: 'actived';
	deleted: 'deleted';
} = {
	actived: 'actived',
	deleted: 'deleted',
};

export class Project {
	private _name: string;
	private _description: string;
	private _status: ProjectStatusType;
	get name(): string {
		return this._name;
	}
	get description(): string {
		return this._description;
	}
	get status(): ProjectStatusType {
		return this._status;
	}
	/**
	 * @description set name for project
	 * @throws InvalidStringInput({ msg: "SPECIAL_CHARACTERS" }) - when string contain special characters
	 * @throws InvalidStringInput({ msg: "EMPTY" }) - when string is ''
	 * @param name
	 */
	set name(name: string) {
		if (name === '') throw new InvalidStringInput('EMPTY');
		if (containsSpecialCharacters(name))
			throw new InvalidStringInput('SPECIAL_CHARACTERS');
		this._name = name;
	}
	/**
	 * @description set description for project
	 * @param des
	 */
	set description(des: string) {
		this._description = des;
	}
	set status(status: ProjectStatusType) {
		this._status = status;
	}
	/**
	 * @throws InvalidStringInput({ msg: "SPECIAL_CHARACTERS" }) - when string contain special characters
	 * @throws InvalidStringInput({ msg: "EMPTY" }) - when string is ''
	 * @throws ProjectException({ msg: "EMPTY_STATUS" }) 
	 */
	validate() {
		if(!this._name) {
			throw new ProjectException('EMPTY_NAME')
		}

		if(!this._status) {
			throw new ProjectException('EMPTY_STATUS')
		}
	}
}
