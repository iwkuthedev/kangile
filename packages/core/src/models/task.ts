import { containsSpecialCharacters } from '../utils/string';
import { User } from './user';
import { InvalidStringInput } from '../exceptions/input.exception';

export type ContentType = {
	time: number;
	blocks: {
		id: string;
		type: string;
		data: any;
	}[];
};

export type StatusType = 'TODO' | 'DOING' | 'DONE';

export class Task {
	private _id: string;
	private _name: string;
	private _description: string;
	private _content: ContentType;
	private _assignTo: User;
	private _status: StatusType;

	/**
	 * @default description = ""
	 * @default content = ""
	 * @default status = "TODO"
	 */
	constructor() {}

	get id(): string {
		return this._id
	}
	get name(): string {
		return this._name;
	}
	get description(): string {
		return this._description;
	}
	get content(): ContentType {
		return this._content;
	}
	get assignTo(): User {
		return this._assignTo;
	}
	get status(): StatusType {
		return this._status;
	}
	set id(id: string) {
		this._id = id
	}

	/**
	 * @description Set name for task
	 * @throws InvalidStringInput({ msg: "SPECIAL_CHARACTERS" }) - when string contain special characters
	 * @throws InvalidStringInput({ msg: "EMPTY" }) - when string is ''
	 */
	set name(str: string) {
		if (containsSpecialCharacters(str))
			throw new InvalidStringInput('SPECIAL_CHARACTERS');
		if ((str === '')) throw new InvalidStringInput('EMPTY');
		this._name = str;
	}

	/**
	 * @description Set description for task
	 */
	set description(str: string) {
		this._description = str;
	}

	/**
	 * @description Set content for task
	 */
	set content(content: ContentType) {
		this._content = content;
	}

	/**
	 * @description Assign task to User
	 * @throws InvalidUserInput({ msg: 'ALREADY_ASSIGNED' }) - when task's assign to someone
	 * @param user
	 */
	assign = (user: User) => {
		this._assignTo = user;
	}

	/**
	 * @description clear assignment of task
	 * @throws TaskException({ msg: 'ASSIGNTO_EMPTY' }) - when task's assign to someone
	 */
	unassign() {}

	/**
	 * @description Change task's status
	 */
	set status(st: StatusType) {
		this._status = st;
	}

	/**
	 * @description Validate all field of model
	 * @throws InvalidField({ msg: "EMPTY", data: { field: ['name'] } }) - when task's name is empty
	 */
	validate = () => {
		if(!this._name) {
			throw new InvalidStringInput('EMPTY_NAME')
		}
	}
}
