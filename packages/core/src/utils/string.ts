export function containsSpecialCharacters(str: string) {
	var regex = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
	return regex.test(str);
}
