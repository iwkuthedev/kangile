export class TaskListException extends Error {
    constructor(msg: string) {
        super(msg)
        this.name = 'TaskListException'
    }
}