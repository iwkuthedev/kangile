export class InvalidStringInput extends Error {
    constructor(msg: string) {
        super(msg)
        this.name = 'InvalidStringInput'
    }
}