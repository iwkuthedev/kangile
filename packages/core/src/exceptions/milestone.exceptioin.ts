export class MileStoneException extends Error {
    constructor(msg: string) {
        super(msg)
        this.name = 'MileStoneException'
    }
}