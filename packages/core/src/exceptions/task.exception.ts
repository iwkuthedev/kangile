export class TaskException extends Error {
    constructor(msg: string) {
        super(msg)
        this.name = 'TaskException'
    }
}