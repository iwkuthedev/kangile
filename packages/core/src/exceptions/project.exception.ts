export class ProjectException extends Error {
    constructor(msg: string) {
        super(msg)
        this.name = 'ProjectException'
    }
}