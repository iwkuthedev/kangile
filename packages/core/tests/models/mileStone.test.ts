import dayjs from 'dayjs';

import { InvalidStringInput } from '../../src/exceptions/input.exception';
import { TaskList } from '../../src/models/taskList';

import { MileStone } from '../../src/models/mileStone';
import { MileStoneException } from '../../src/exceptions/milestone.exceptioin';

describe('set name for mile stone', () => {
	it('set name for milestone', () => {
		const mileStone = new MileStone();
		mileStone.name = 'MileStone name';

		expect(mileStone.name).toEqual('MileStone name');
	});

	it(`throw error when string is ''`, () => {
		const mileStone = new MileStone();

		expect(() => (mileStone.name = '')).toThrow(
			new InvalidStringInput('EMPTY'),
		);
	});
});

describe('set openDate for milestone', () => {
	it('set opendate for mileStone', () => {
		const mileStone = new MileStone();

		const openDate = dayjs('12-12-2023');

		mileStone.openDate = openDate;

		expect(mileStone.openDate.format()).toEqual(openDate.format());
	});

	it(`throw MileStoneException('DEALINE_IS_BEFORE_OPENDATE')`, () => {
		const mileStone = new MileStone();
		const openDate = dayjs('12-12-2023');
		const deadline = dayjs('12-11-2023');
		mileStone.deadline = deadline;

		expect(() => (mileStone.openDate = openDate)).toThrow(
			new MileStoneException('DEADLINE_IS_BEFORE_OPENDATE'),
		);
	});
});

describe('set deadline for milestone', () => {
	it('set deadline for milestone', () => {
		const milestone = new MileStone();
		const deadline = dayjs('12-12-2023');
		milestone.deadline = deadline;

		expect(milestone.deadline.format()).toEqual(deadline.format());
	});

	it(`throw MileStoneException('DEALINE_IS_BEFORE_OPENDATE')`, () => {
		const mileStone = new MileStone();
		const openDate = dayjs('12-12-2023');
		const deadline = dayjs('12-11-2023');
		mileStone.openDate = openDate;

		expect(() => (mileStone.deadline = deadline)).toThrow(
			new MileStoneException('DEADLINE_IS_BEFORE_OPENDATE'),
		);
	});
});

describe('add tasklist into milestone', () => {
	it('add tasklist into milestone', () => {
		const milestone = new MileStone();
		const taskList = new TaskList();
		taskList.id = '123';
		taskList.name = 'tasklist 1';

		milestone.addTaskList(taskList);
		expect(milestone.taskLists.some(item => item.id === '123')).toEqual(
			true,
		);
	});

	it('tasklist is validated before add into milestone', () => {
		const milestone = new MileStone();
		const taskList = new TaskList();
		taskList.name = 'tasklist 1';

		const spy = jest.spyOn(taskList, 'validate');
		milestone.addTaskList(taskList);

		expect(spy).toHaveBeenCalled();
	});

	it(`throw MileStoneError('TASKLIST_EXISTED') when tasklist is already existed`, () => {
		const milestone = new MileStone();
		const taskList = new TaskList();
		taskList.id = 'abc';
		taskList.name = 'Tasklist 1';

		milestone.addTaskList(taskList);

		expect(() => milestone.addTaskList(taskList)).toThrow(
			new MileStoneException('TASKLIST_EXISTED'),
		);
	});
});

describe('remove tasklsit from milestone', () => {
	it('remove tasklsit from milestone', () => {
		const milestone = new MileStone();
		const taskList = new TaskList();
		taskList.id = 'abc';
		taskList.name = 'Tasklist 1';
		milestone.addTaskList(taskList);

		milestone.removeTaskList(taskList.id);

		expect(
			milestone.taskLists.some(item => item.id == taskList.id),
		).toBeFalsy();
	});

	it(`MileStoneError('TASKLIST_NOT_FOUND') - when taskList not found in milestone`, () => {
		const milestone = new MileStone();
		const taskList = new TaskList();
		taskList.id = 'abc';
		taskList.name = 'Tasklist 1';
		milestone.addTaskList(taskList);

		expect(() => milestone.removeTaskList('not abc')).toThrow(new MileStoneException('TASKLIST_NOT_FOUND'))
	});
});
