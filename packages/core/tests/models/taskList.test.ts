import { Task } from '../../src/models/task';
import { InvalidStringInput } from '../../src/exceptions/input.exception';
import { TaskList } from '../../src/models/taskList';
import { TaskListException } from '../../src/exceptions/taskList.exception';

describe('set id for taskList', () => {
	it('set id for taskList', () => {
		const taskList = new TaskList();
		taskList.id = '123';

		expect(taskList.id).toEqual('123');
	});
});

describe('set name for taskList', () => {
	it('set name for taskList', () => {
		const taskList = new TaskList();
		taskList.name = 'Task list A';

		expect(taskList.name).toEqual('Task list A');
	});
	it('@throws InvalidStringInput({ msg: "EMPTY" }) - when string is', () => {
		const taskList = new TaskList();

		expect(() => (taskList.name = '')).toThrow(
			new InvalidStringInput('EMPTY'),
		);
	});
	it('@throws InvalidStringInput({ msg: "EMPTY" }) - when string is', () => {
		const taskList = new TaskList();

		expect(() => (taskList.name = ' ')).toThrow(
			new InvalidStringInput('EMPTY'),
		);
	});
});

describe('add task to taskList', () => {
	it('add task to tasklist', () => {
		const taskList = new TaskList();
		const task = new Task();
		task.id = 'abc';
		task.name = 'task 1';

		taskList.addTask(task);

		expect(taskList.tasks.some(item => item.id === task.id));
	});
	it(`task's status is set to 'TODO'`, () => {
		const taskList = new TaskList();
		const task = new Task();
		task.id = 'abc';
		task.name = 'task 1';

		taskList.addTask(task);

		expect(
			taskList.tasks.find(item => (item.id = task.id))?.status,
		).toEqual('TODO');
	});

	it('Validate task before add into taskList', () => {
		const taskList = new TaskList();
		const task = new Task();
		task.id = 'abc';
		task.name = 'task 1';
		const spy = jest.spyOn(task, 'validate');

		taskList.addTask(task);

		expect(spy).toHaveBeenCalled();
	});
});

describe('remove task from taskList', () => {
	it('remove taks from tasklist', () => {
		const taskList = new TaskList();
		const task = new Task();
		task.id = 'abc';
		task.name = 'task 1';

		taskList.addTask(task);

		taskList.removeTask(task.id);

		expect(taskList.tasks.some(item => item.id === task.id)).toBeFalsy();
	});

	it(`throws TaskException('TASK_NOT_FOUND')`, () => {
		const taskList = new TaskList();
		const task = new Task();
		task.id = 'abc';
		task.name = 'task 1';

		taskList.addTask(task);

		expect(() => taskList.removeTask('not id')).toThrow(
			new TaskListException('TASK_NOT_FOUND'),
		);
	});
});

describe('validate', () => {
	it(`Throw TaskListException('NAME_EMPTY')`, () => {
		const taskList = new TaskList();

		expect(() => taskList.validate()).toThrow(
			new TaskListException('NAME_EMPTY'),
		);
	});
});
