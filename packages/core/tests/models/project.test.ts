import { InvalidStringInput } from '../../src/exceptions/input.exception';
import { PROJECT_STATUS, Project } from '../../src';
import { ProjectException } from '../../src/exceptions/project.exception';

describe('set name for Project', () => {
	it('set name for project', () => {
		const project = new Project();

		project.name = 'Project V';

		expect(project.name).toEqual('Project V');
	});
	it('@throws InvalidStringInput({ msg: "SPECIAL_CHARACTERS" }) - when string contain special characters', () => {
		const project = new Project();

		expect(() => {
			project.name = 'Project V @';
		}).toThrow(new InvalidStringInput('SPECIAL_CHARACTERS'));
	});
	it('@throws InvalidStringInput({ msg: "EMPTY" }) - when string is ""', () => {
		const project = new Project();

		expect(() => {
			project.name = '';
		}).toThrow(new InvalidStringInput('EMPTY'));
	});
});

it('set description for project', () => {
	const project = new Project();

	project.description = 'description';

	expect(project.description).toEqual('description');
});

it('set status for project', () => {
	const project = new Project();

	project.status = PROJECT_STATUS.actived;

	expect(project.status).toEqual(PROJECT_STATUS.actived);

	project.status = PROJECT_STATUS.deleted;

	expect(project.status).toEqual(PROJECT_STATUS.deleted);
});

describe('validate all properties of project', () => {
	it('happy case', () => {
		const project = new Project();

		project.name = 'Project V';
		project.description = 'Description of project V';
		project.status = PROJECT_STATUS.actived;

		const validateFn = jest.fn(() => project.validate());
		validateFn();

		expect(validateFn.mock.calls).toHaveLength(1);
	});

	it(`ProjectException({ msg: "EMPTY_NAME" }) - name is empty`, () => {
		const project = new Project();

		project.description = 'Description of project V';
		project.status = PROJECT_STATUS.actived;

		expect(() => project.validate()).toThrow(new ProjectException("EMPTY_NAME"))
	});

	it(`ProjectException({ msg: "EMPTY_STATUS" }) `, () => {
		const project = new Project();

		project.name = 'Project V';
		project.description = 'Description of project V';

		expect(() => project.validate()).toThrow(new ProjectException("EMPTY_STATUS"))
	});
});
