import { InvalidStringInput } from "../../src/exceptions/input.exception"
import { User } from "../../src/models/user"

describe('set username for user', () => {
    it('happy case' ,() => {
        const user = new User()

        user.username = 'user A'

        expect(user.username).toEqual('user A')
    })

    it(`validUserInput({ msg: 'EMPTY' }) - when user's name os empty`, () => {
        const user = new User()

        expect(() => user.username = "").toThrow(new InvalidStringInput('EMPTY'))
    })
})

describe('set status', () => {
    it('set status for user', () => {
        const user = new User()
        user.status = 'actived'
        expect(user.status).toEqual('actived')

        user.status = 'banned'
        expect(user.status).toEqual('banned')

        user.status = 'disactive'
        expect(user.status).toEqual('disactive')
    })

    it(`default value is 'actived'`, () => {
        const user = new User()

        expect(user.status).toEqual('actived')
    })
})

describe('set avatar for user', () => {
    it('set avatar for user', () =>{
        const user = new User()
        user.avatar = 'link to avatar'
        expect(user.avatar).toEqual('link to avatar')
    })
})