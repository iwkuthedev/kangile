/**
 * @description Set name for task
 * @throws InvalidStringInput({ msg: "SPECIAL_CHARACTERS" }) - when string contain special characters
 * @throws InvalidStringInput({ msg: "EMPTY" }) - when string is ''
 */

import { InvalidStringInput } from '../../src/exceptions/input.exception';
import { Task } from '../../src/models/task';

const fakeTaskContent = {
	time: 1700383715975,
	blocks: [
		{
			id: 'mhTl6ghSkV',
			type: 'paragraph',
			data: {
				text: 'Hey. Meet the new Editor. On this picture you can see it in action. Then, try a demo 🤓',
			},
		},
		{
			id: 'l98dyx3yjb',
			type: 'header',
			data: {
				text: 'Key features',
				level: 3,
			},
		},
		{
			id: 'os_YI4eub4',
			type: 'list',
			data: {
				type: 'unordered',
				items: [
					'It is a block-style editor',
					'It returns clean data output in JSON',
					`Designed to be extendable and pluggable with a <a href="https://editorjs.io/creating-a-block-tool">simple API</a>" `,
				],
			},
		},
	],
};

describe('set name for task', () => {
	it('happy case', () => {
		const task = new Task();

		task.name = 'Task A';

		expect(task.name).toEqual('Task A');
	});

	it(`InvalidStringInput({ msg: "SPECIAL_CHARACTERS" }) - when string contain special characters`, () => {
		const task = new Task();

		expect(() => {
			task.name = '$a as $';
		}).toThrow(new InvalidStringInput('SPECIAL_CHARACTERS'));
	});

	it(`@throws InvalidStringInput({ msg: "EMPTY" }) - when string is ''`, () => {
		const task = new Task();

		expect(() => {
			task.name = '';
		}).toThrow(new InvalidStringInput('EMPTY'));
	});
});

describe('set description for task', () => {
	it('happy case', () => {
		const task = new Task();

		task.description = 'description of task';

		expect(task.description).toEqual('description of task');
	});
});

describe('set content for task', () => {
	it('happy case', () => {
		const task = new Task();

		task.content = fakeTaskContent;

		expect(task.content).toEqual(fakeTaskContent);
	});
});

describe('validate all info of task', () => {
	 it(`@throws InvalidField('EMPTY_NAME') - when task's name is empty`, () => {
		const task = new Task()
		expect(() => task.validate()).toThrow(new InvalidStringInput('EMPTY_NAME'))
	 })
})

