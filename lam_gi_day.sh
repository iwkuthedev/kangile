deploy(){
    COMPOSE_PROFILES=deploy COMPOSE_PROJECT_NAME="directus-${CI_COMMIT_SHA}" \
        docker compose --file=./.deploy/docker-compose.yaml up 
}